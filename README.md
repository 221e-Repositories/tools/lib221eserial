# 221e Communication Protocol

C++ library for Serial Communication.

Package available for:

 - x64 Windows 10 
 - Ubuntu 20.04 LTS

## Dependencies
 - If you are using this library with ROS for Windows, remember to start your installation routine from the ROS command short cut.

## Quick start (Ubuntu 20.04 LTS version)

Open a terminal and, after positioning in the desired folder,
```sh
git clone https://gitlab.com/221e-Repositories/tools/lib221eserial.git
cd lib221eSerial
mkdir build
cd build
cmake ..
make
sudo make install
```

## Quick start (Windows 10 version)

- Clone the library repository into your desired folder;
- Open the project folder using your preferred C/C++ editor;
- Configure and build the project;
- The default installation folder are bin and lib for executables and libraries, respectively.

(*Note: if you are using this library with ROS for Windows, remember to start your installation routine from the ROS command short cut.*)
